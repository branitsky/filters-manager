FROM golang:1.6-onbuild

ENV PORT=8086

RUN mkdir -p /go/src/app
COPY . /go/src/app
WORKDIR /go/src/app

RUN go build -o main .

# EXPOSE $PORT

CMD ["/go/src/app/main"]