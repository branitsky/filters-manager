package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gopkg.in/gcfg.v1"
)

// Config represents a configuration parameters.
type Config struct {
	Main struct {
		Port string
	}
	Urls struct {
		Original string
		Reformed string
	}
}

// ReadConfig gets the settings from the gcfg file.
func ReadConfig(filepath string) Config {
	var config Config

	err := gcfg.ReadFileInto(&config, filepath)
	if err != nil {
		fmt.Println(err.Error())
		return config
	}
	return config
}

// ReadJSONConfig gets the settings from the JSON file. For development purposes only.
func ReadJSONConfig() Config {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %v <configfile>\n", os.Args[0])
		os.Exit(1)
	}

	extension := filepath.Ext(os.Args[1])
	if extension != ".json" {
		log.Fatal("It seems like, this is not a JSON file.")
	}

	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	var config Config

	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&config); err != nil {
		log.Fatal(err)
	}
	return config
}
