package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const (
	// CONFPATH contains the path to the conf file.
	CONFPATH string = "../conf.gcfg"
	// URL1 test url
	URL1 string = "http://localhost:8085"
	// URL2 test url
	URL2 string = "http://localhost:8086"
)

type (
	// ClientRequest represents a request to start work.
	ClientRequest struct {
		URL string
	}

	// Response represents a server/client response.
	Response struct {
		Message string
	}

	// Urls represents a url's which will be sent on request.
	Urls struct {
		Original string
		Reformed string
	}
)

// SendRequest sends a request to another server.
func SendRequest(url string) *http.Response {
	conf := ReadConfig(CONFPATH)

	urls := &Urls{Original: conf.Urls.Original, Reformed: conf.Urls.Reformed}
	body := new(bytes.Buffer)
	json.NewEncoder(body).Encode(urls)
	resp, err := http.Post(url, "application/json; charset=utf-8", body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(resp)
	return resp
}

func Handle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var req ClientRequest

	if r.Method == "POST" {
		contentType := r.Header.Get("Content-Type")
		if contentType != "application/json" {
			response := Response{"Unsupported media type."}
			j, _ := json.Marshal(response)

			w.WriteHeader(http.StatusUnsupportedMediaType)
			w.Write(j)
			return
		}

		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&req); err != nil {
			log.Fatal(err)
		}
	} else {
		response := Response{"Method not allowed."}
		j, _ := json.Marshal(response)

		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write(j)
		return
	}

	SendRequest(URL1)
}

func main() {
	conf := ReadConfig(CONFPATH)
	PORT := conf.Main.Port

	http.HandleFunc("/", Handle)
	fmt.Println("Server is running on", PORT, "\nPress CTRL+C to shutdown.")
	log.Fatal(http.ListenAndServe(":"+PORT, nil))
}
