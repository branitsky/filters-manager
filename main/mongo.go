package main

import (
	"log"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	MongoDBHost string = "mongodb://localhost:27017"
	Database    string = "reports"
	Collection  string = "regression"
)

/*
Document represent a mongodb document
to be written to the database.
*/
type Document struct {
	ID bson.ObjectID
}

func getSession() *mgo.Session {
	session, err := mgo.Dial(MongoDBHost)
	if err != nil {
		log.Fatal(err)
	}
	return session
}

func insertOne(filename string, Document document) {
	d := &Document{}
	s := getSession()

	defer s.Close()

	// Add an ObjectID
	d.ID = bson.NewObjectId()
	if err := s.DB(Database).C(Collection).Insert(document); err != nil {
		// fmt.Println("")
		log.Fatal(err)
	}
}

func updateOne(filename string, Document document) {

}
